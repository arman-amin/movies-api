# image-resize

## Usage
install laravel and packages
```php
composer install
```

rename .env.example to .env and change CACHE_DRIVER value to redis.

run this command to start api server
```php
php artisan serve
```
