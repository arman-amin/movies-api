<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class SearchController extends Controller
{

    public function search(Request $request){
        $q = $request->get('q');

        $tag_items = Redis::sinter($q);

        if(count($tag_items) > 0){

            $result = [];

            foreach($tag_items as $item){
                $result[] = json_decode(Redis::get($item), true);
            }
            
            return response()->json([
                'items' => $result
            ]);
        }

        $client = new Client();

        $response = $client->get(config('moviesapi.search_url') . '?q=' . $q);

        $data = json_decode($response->getBody()->getContents(), true);

        foreach($data['data'] as $item){
            Redis::sadd($q, $item['id']);
            Redis::set($item['id'], json_encode($item));
        }

        return response()->json([
            'items' => $data['data']
        ]);
    }
}
